    /*-
     * Copyright (c) 2013 Leonardo Pepe de Freitas.
     * All rights reserved.
     *
     * This code is derived from software contributed to Leonardo Pepe de Freitas
     * by
     *
     * Redistribution and use in source and binary forms, with or without
     * modification, are permitted provided that the following conditions
     * are met:
     * 1. Redistributions of source code must retain the above copyright
     *    notice, this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright
     *    notice, this list of conditions and the following disclaimer in the
     *    documentation and/or other materials provided with the distribution.
     *
     * THIS SOFTWARE IS PROVIDED BY Leonardo Pepe de Freitas. AND CONTRIBUTORS
     * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
     * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
     * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
     * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
     * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
     * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
     * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
     * POSSIBILITY OF SUCH DAMAGE.
     */

     =========*****=======
            (_ )
         \\\", ) ^
           \/, \(
           CXC_/_)
             Leonardo Pepe
     =========*****=======
     bitbucket.org/leopepe
     github.com/leopepe
     =========*****=======

     Created at: 2013-06-26
     Author: Leonardo Pepe de Freitas
     Cod-name: [Acanthophis] (http://en.wikipedia.org/wiki/Acanthophis)
     Last updated: 2013-10-31
     version: 2.0


HTTPREquester
==================


ABOUT
-----

This project creates an http client (requester) capable of sending http request using the standard HTTP/1.1 verbs
(GET, POST, UPDATE, PUT and DELETE) with modified headers, body or any HTTP/1.1 protocol element.


Motivations
-----------

The main motivations on building this app is to create a flexible and easy tool for testing web services (soap)
and RESTful web applications.


Features
--------

* CLI Interface
* Implements HTTP 1.1 default verbs (get, post, put, update, delete)
* Support RESTFul web server tests
* Reads an xml file containing the xml soap envelop (-s options on CLI)


Usage
-----

You can use HTTPRequester onto your own scripts or via CLI

### Class Usage

    header = {
        'KeepAlive': 'On',
        'Content-Type': 'text/html'
    }

    requester = HTTPRequestFacade(host="www.google.com", port=80)
    status, reason, response = requester.get(uri='/', body='', headers=header)

    print("\n## Printing from class get response facades ##")
    print requester.get_status()
    print requester.get_reason()
    print requester.get_response()

    print("\n## Printing from local variables ##")
    print status
    print reason
    print response


### CLI Usage

Heather then use the HTTPRequester on your own scripts you can use it as a complied client tool.

#### Windows

    c:\Python27\python.exe main.py -s soap_envelop.xml -p 80 -m GET -u "http://www.google.com/q?arg1=arg1&arg2=value2"

#### Linux

    $ python main.py -s soap_envelop.xml -p 80 -m GET -u "http://www.google.com/q?arg1=arg1&arg2=value2"