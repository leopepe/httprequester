#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Module Docstring
Docstrings: http://www.python.org/dev/peps/pep-0257/
"""

__author__ = "Leonardo Pepe de Freitas"
__license__ = "Simplified BSD License"
__copyright__ = "Copyright (c) 2013, Leonardo Pepe de Freitas"
__version__ = "1.0"


from httplib import HTTPConnection


class HTTPStandardMethods(object):

    def http_default_attrib(**kwargs):
        """
        :args: host, port, uri, headers
        :rtype: readable http response
        """
        host = kwargs.get('host', 'localhost')
        port = kwargs.get('port', 80)
        method = kwargs.get('method', 'GET')
        uri = kwargs.get('uri', '/')
        body = kwargs.get('body', None)
        headers = kwargs.get('headers', '{"KeepAlive" : "On"}')


class HTTPRequester(object):

    @classmethod
    def connector(cls, host, port):
        """
        :args: host, port
        :rtype: http connection object
        pre: host and port arguments must be passed
        post: return an HTTP Connection object
        """
        return HTTPConnection(host, port)

    @classmethod
    def requester(cls, **kwargs):
        """
        :args: host, port, method, uri, headers
        :rtype: http response status, reason and response readable data
        pre: connector method must successful return an http conn objc
        post: returns a readable http response
        """
        host = kwargs.get('host', 'localhost')
        port = kwargs.get('port', 80)
        method = kwargs.get('method', 'GET')
        uri = kwargs.get('uri', '/')
        body = kwargs.get('body')
        headers = kwargs.get('headers', '{"KeepAlive" : "On"}')

        conn = cls.connector(host=host, port=port)
        conn.request(method=method, url=uri, body=body, headers=headers)
        response = conn.getresponse()
        return response.status, response.reason, response.read()

    @classmethod
    def get(cls, host, port, uri, headers):
        """
        :args: host, port, uri, headers
        :rtype: readable http response
        """
        return cls.requester(host=host, port=port, method='GET', uri=uri, headers=headers)

    @classmethod
    def post(cls, host, port, uri, headers):
        """
        :args: host, port, uri, headers
        :rtype: readable http response
        """
        return cls.requester(host=host, port=port, method='post', uri=uri, headers=headers)

    @classmethod
    def put(cls, host, port, uri, headers):
        """
        :args: host, port, uri, headers
        :rtype: readable http response
        """
        return cls.requester(host=host, port=port, method='put', uri=uri, headers=headers)

    @classmethod
    def update(cls, host, port, uri, headers):
        """
        :args: host, port, uri, headers
        :rtype: readable http response
        """
        return cls.requester(host=host, port=port, method='update', uri=uri, headers=headers)

    @classmethod
    def delete(cls, host, port, uri, headers):
        """
        :args: host, port, uri, headers
        :rtype: readable http response
        """
        return cls.requester(host=host, port=port, method='update', uri=uri, headers=headers)


class HTTPSRequester(HTTPRequester):
    pass


if __name__ == '__main__':
    header = {
        'KeepAlive': 'On',
        'Content-Type': 'text/html'
    }
    get_response = HTTPRequester.get(host='www.google.com', port=80, uri='/', headers=header)
    print get_response