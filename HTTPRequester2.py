#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Module Docstring
Docstrings: http://www.python.org/dev/peps/pep-0257/
"""

__author__ = "Leonardo Pepe de Freitas"
__license__ = "Simplified BSD License"
__copyright__ = "Copyright (c) 2013, Leonardo Pepe de Freitas"
__version__ = "2.0"


from httplib import HTTPConnection
from urlparse import urljoin


class HTTPRequester(object):

    def __init__(self, host, port):
        """
        :args: host, port
        :rtype: readable http response
        """
        # Initialize all attributes
        self.host = host
        self.port = port
        self.method = None
        self.uri = None
        self.body = None
        self.headers = None
        self.request_response = None
        self.request_response_data = None
        # If host and port is not passed as arg raise NotImplementedError
        # *force the connection to be established first
        if self.host is not None and self.port is not None:
            self.connection = self.connector(self.host, self.port)
        else:
            raise "Connection not established %s" % NotImplementedError

    def connector(self, host, port):
        """
        :args: host, port
        :rtype: http connection object
        pre: host and port arguments must be passed
        post: return an HTTP Connection object
        """
        # only creates the conn object
        self.connection = HTTPConnection(host, port)
        return self.connection

    def requester(self, **kwargs):
        """
        :args: host, port, method, uri, headers
        :rtype: http response status, reason and response readable data
        pre: connector method must successful return an http conn objc
        post: returns a readable http response
        """
        # Set the http.request attributes method, uri, body e headers
        self.method = kwargs.get('method')
        self.uri = kwargs.get('uri')
        self.body = kwargs.get('body')
        self.headers = kwargs.get('headers', '{"KeepAlive" : "On"}')
        # Ensure that the connection object exists
        if self.connection is not None:
            self.connection.request(method=self.method, url=self.uri, body=self.body, headers=self.headers)
            self.request_response = self.connection.getresponse()
            # If status equal to 301, 302 or 307 (redirect) reconnects and request to moved url
            if self.request_response.status in (301, 302, 307):
                # Set moved_to by getting response.getheader('location')
                moved_to = urljoin(self.uri, self.request_response.getheader('location', ''))
                # Redo connection
                self.connector(self.host, self.port)
                self.connection.request(method=self.method, url=moved_to, body=self.body, headers=self.headers)
                self.request_response = self.connection.getresponse()
            self.request_response_data = self.request_response.read()
            return self.request_response.status, self.request_response.reason, self.request_response_data
        else:
            raise "Connection not established %s" % NotImplementedError

    def get_response(self):
        # If there's a response to be return, return status, reason and response data.
        if self.request_response is not None:
            return self.request_response.status, self.request_response.reason, self.request_response_data
        elif NotImplementedError:
            raise "No http response was found. Probably there's no HTTP Connection established %s" % NotImplementedError

    def get_reason(self):
        # If there's a response to be return, return reason
        if self.request_response is not None:
            return self.request_response.reason
        elif NotImplementedError:
            raise "No http response was found. Probably there's no HTTP Connection established %s" % NotImplementedError

    def get_status(self):
        # If there's a response to be return, return status
        if self.request_response is not None:
            return self.request_response.status
        elif NotImplementedError:
            raise "No http response was found. Probably there's no HTTP Connection established %s" % NotImplementedError


class HTTPMethodsInterface(HTTPRequester):

    def get(self, **kwargs):
        """
        :args: host, port, uri, headers
        :rtype: readable http response
        """
        uri = kwargs.get('uri')
        headers = kwargs.get('headers')
        body = kwargs.get('body')
        return self.requester(method='GET', body=body, uri=uri, headers=headers)

    def post(self, **kwargs):
        """
        :args: host, port, uri, headers
        :rtype: readable http response
        """
        uri = kwargs.get('uri')
        headers = kwargs.get('headers')
        body = kwargs.get('body')
        return self.requester(method='POST', body=body, uri=uri, headers=headers)

    def put(self, **kwargs):
        """
        :args: host, port, uri, headers
        :rtype: readable http response
        """
        uri = kwargs.get('uri')
        headers = kwargs.get('headers')
        body = kwargs.get('body')
        return self.requester(method='PUT', body=body, uri=uri, headers=headers)

    def update(self, **kwargs):
        """
        :args: host, port, uri, headers
        :rtype: readable http response
        """
        uri = kwargs.get('uri')
        headers = kwargs.get('headers')
        body = kwargs.get('body')
        return self.requester(method='UPDATE', body=body, uri=uri, headers=headers)

    def delete(self, **kwargs):
        """
        :args: host, port, uri, headers
        :rtype: readable http response
        """
        uri = kwargs.get('uri')
        headers = kwargs.get('headers')
        body = kwargs.get('body')
        return self.requester(method='DELETE', body=body, uri=uri, headers=headers)


if __name__ == '__main__':
    header = {
        'KeepAlive': 'On',
        'Content-Type': 'text/html'
    }
    """
    http = HTTPRequester(host='www.google.com', port=80)
    print http.requester(method='GET', uri='/', body='', headers=header)
    response = http.get_response()
    print response
    """
    requester = HTTPMethodsInterface(host="www.google.com", port=80)
    status, reason, response = requester.get(uri='/', body='', headers=header)
    print("\n## Printing from class get response facades ##")
    print requester.get_status()
    print requester.get_reason()
    print requester.get_response()

    print("\n## Printing from local variables ##")
    print status
    print reason
    print response