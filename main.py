__author__ = 'leonardo.pepe'

from HTTPRequester2 import HTTPMethodsInterface
from argparse import ArgumentParser


def opt_parser():
    parser = ArgumentParser()
    parser.add_argument(
                        "-s", "--soap",
                        action="store",
                        dest="soap_file",
                        help="read soap envelop xml file",
    )
    parser.add_argument(
                        "-u", "--url",
                        action="store",
                        dest="url",
                        help="read soap envelop xml file"
    )
    parser.add_argument(
                        "-m", "--method",
                        action="store",
                        dest="method",
                        help="http method to be used"
    )
    parser.add_argument(
                        "-p", "--port",
                        action="store",
                        dest="port",
                        help="http server port"
    )
    return parser.parse_args()


def main():
    # call opt_parser() function to set args
    args = opt_parser()
    # open soap_file to get soap envelop content
    # closes it after reading the content
    if args.soap_file is not None:
        soap_envelop = open(args.soap_file, 'r')
        body = soap_envelop.read()
        soap_envelop.close()
    else:
        body = ''
    # check passed protocol if https raise NotImplementedError
    if args.url[:5] == 'https':
        raise NotImplementedError, "Https not supported"
    elif args.url[:4] == 'http':
        proto, empty, host, uri = args.url.split('/')

    headers = {
        'KeepAlive': 'On',
        'Content-Type': 'text/html'
    }
    # Instantiate HTTPRequestFacade() to connect
    http_client = HTTPMethodsInterface(host, args.port)
    # Check the http method to be used
    if args.method == 'get' or 'GET':
        http_client.get(uri=uri, body=body, headers=headers)
    elif args.method == 'post' or 'POST':
        http_client.post(uri=uri, body=body, headers=headers)
    elif args.method == 'put' or 'PUT':
        http_client.put(uri=uri, body=body, headers=headers)
    elif args.method == 'update' or 'UPDATE':
        http_client.update(uri=uri, body=body, headers=headers)
    elif args.method == 'delete' or 'DELETE':
        http_client.update(uri=uri, body=body, headers=headers)

    print http_client.get_response()

if __name__ == '__main__':
    main()